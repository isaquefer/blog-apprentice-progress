param($project_start_mode)

if ($project_start_mode -eq "prod") {
    .\bin\windows\init_prod.ps1
} ElseIf ($project_start_mode -eq "dev") {
    .\bin\windows\init.ps1
} else {
    write-host "Passe como parametro 'prod' ou 'dev' para iniciar o projeto"
}
