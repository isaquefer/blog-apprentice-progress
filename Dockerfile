FROM nginx

RUN rm /etc/nginx/conf.d/default.conf
RUN rm /usr/share/nginx/html/index.html

ADD docker_files/default.conf /etc/nginx/conf.d/default.conf
ADD dist /usr/share/nginx/html
