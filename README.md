# blog-apprentice-progress

## Pre-requisites
- node
- NPM
- docker
- docker-compose

## Start development project on linux
```
make

```

## Start development project on windows
```
make run_prod

```

## Start production project on windows
```
.\init.ps1 prod

```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
